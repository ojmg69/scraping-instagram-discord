require('dotenv').config();
import express from 'express';
import configViewEngine from './src/config/viewEngine';
import initialWebRoutes from './src/routes/web';
import bodyParser from 'body-parser';

let app= express();
//config body-parser to post data
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

//config view engine
configViewEngine(app);

//init web routes
initialWebRoutes(app);

let port =process.env.PORT || 5000;
app.listen(port,()=>{
  console.log(`Messenger ChatBot run on ${port}`);
});
